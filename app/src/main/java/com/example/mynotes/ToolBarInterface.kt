package com.example.mynotes

interface ToolBarInterface {
    fun setMyTitle(title: String)
    fun disabelToolBar()
    fun enableToolBar()
}