package com.example.mynotes

import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.squareup.okhttp.internal.Internal.logger
import java.util.logging.Level

class SocketRepository {
    private var socket: Socket
    private val uri = "http://192.168.1.156:2000"
    var isConnected: Boolean = false

    init {
        socket = IO.socket(uri)
    }
    fun addListener(key: String, listener: Emitter.Listener) {
        socket.on(key, listener)
    }

    fun sendNoteToServer(note: String) {
        socket.emit("insert_note", note)
        logger.log(Level.INFO, "Inserted note")
    }

    fun sendAllNotesToServer(notes: String) {
        socket.emit("setup_notes", notes)
    }

    fun sendAllUsersToServer(users: String) {
        socket.emit("setup_users", users)
    }

    fun updateNoteInServer(note: String) {
        socket.emit("update_note", note)
    }

    fun deleteNoteInServer(note: String) {
        socket.emit("delete_note", note)
    }

    fun sendUserToServer(user: String) {
        socket.emit("insert_user", user)
    }

    fun connect() {
        socket.connect()
        isConnected = true
        logger.log(Level.INFO, "Socket is connected")
    }

    fun disconnect() {
        socket.disconnect()
        logger.log(Level.INFO, "Disconnected socket")
        isConnected = false
    }

}
