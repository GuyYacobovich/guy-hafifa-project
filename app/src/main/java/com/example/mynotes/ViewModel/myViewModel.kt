package com.example.mynotes.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mynotes.Notes.Note
import com.example.mynotes.Notes.NoteRepository
import com.example.mynotes.SharedPref
import com.example.mynotes.SocketRepository
import com.example.mynotes.Users.User
import com.example.mynotes.Users.UserRepository
import com.example.mynotes.Users.UsersNotes
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject


class myViewModel @Inject constructor(
    private val notesRepository: NoteRepository,
    private val userRepository: UserRepository,
    private val sharedPref: SharedPref,
    private val socketRepository: SocketRepository)
    : ViewModel() {

    fun insertNote(note: Note): Long {
        return runBlocking {
            notesRepository.insert(note)
        }
    }

    fun deleteNote(note: Note?) {
        viewModelScope.launch { note?.let { notesRepository.delete(note) } }
    }

    fun updateNote(note: Note) {
        viewModelScope.launch { notesRepository.update(note) }
    }

    fun findNoteById(noteId: Long): Note? {
        return runBlocking {
            notesRepository.findNoteById(noteId)
        }
    }

    fun getUserNotes(): LiveData<List<UsersNotes>?> {
        return notesRepository.getUserNotes()
    }

    fun getUserById(userId: Long): User? {
        return runBlocking {
            userRepository.getUserById(userId)
        }
    }

    fun insertUser(user: User): Long {
        return runBlocking {
            userRepository.insert(user)
        }
    }

    fun updateUser(user: User) {
        viewModelScope.launch { userRepository.update(user) }
    }


    fun getUserByUsername(username: String): User? {
        return runBlocking {
            userRepository.getUserByUsername(username)
        }
    }

    fun loginUser(username: String) {
        viewModelScope.launch {
            val currentUser = getUserByUsername(username)
            if (currentUser != null) {
                sharedPref.loginUser(currentUser.userName)
            } else {
                insertUser(User(0, username))
                sharedPref.loginUser(username)
            }
        }
    }

    fun logoutUser() {
        sharedPref.logoutUser()
    }

    fun getLoggedUser(): String? {
        return sharedPref.getLoggedUser()
    }

    fun updateLoggedUser(username: String) {
        sharedPref.updateLoggedUsername(username)
    }

    fun updateLoggedUserId(userId: Long) {
        sharedPref.updateLoggedUserId(userId)
    }

    fun getLoggedUserId(): Long {
        return sharedPref.getLoggedUserId()
    }

    fun socketInsertNote(note: Note) {
        val noteId: Long
        noteId = insertNote(note)
        note.id = noteId
        socketRepository.sendNoteToServer(ObjectMapper().writeValueAsString(note))
    }

    fun socketUpdateNote(note: Note) {
        updateNote(note)
        socketRepository.updateNoteInServer(ObjectMapper().writeValueAsString(note))
    }

    fun socketSendUser(user: User) {
        socketRepository.sendUserToServer(ObjectMapper().writeValueAsString(user))
    }

    fun socketDeleteNote(note: Note) {
        deleteNote(note)
        socketRepository.deleteNoteInServer(ObjectMapper().writeValueAsString(note))
    }

    fun socketIsConnected(): Boolean? {
        return socketRepository.isConnected
    }
}