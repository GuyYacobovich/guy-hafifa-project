package com.example.mynotes

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mynotes.Adapter.NotesAdapter
import com.example.mynotes.Notifications.NotificationInterface
import com.example.mynotes.ViewModel.myViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_reminders.*
import java.util.*
import javax.inject.Inject

class RemindersFragment : Fragment(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var mContext: Context
    lateinit var viewModel: myViewModel
    private lateinit var adapter: NotesAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_reminders, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(myViewModel::class.java)

        adapter = NotesAdapter {
            val action =
                RemindersFragmentDirections.actionRemindersFragment2ToDetailsFragment(it.id)
            view?.findNavController()?.navigate(action)
        }
        val userId = viewModel.getLoggedUserId()
        viewModel.getUserNotes().observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.setData(
                    it.find { it.user.userId == viewModel.getLoggedUserId() }?.notes ?: emptyList()
                )

                cancelAllNotifications(userId)
                createAllNotifications(userId)

            }
        })

        recycler_view.adapter = adapter
        recycler_view.layoutManager = LinearLayoutManager(activity)
        recycler_view.setHasFixedSize(true)

        val itemTouchHelperCallback =
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int) {
                    val note = (viewHolder as? NotesAdapter.myViewHolder)?.data
                    note?.let { viewModel.socketDeleteNote(it) }
                    viewModel.deleteNote(note)
                }
            }
        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(recycler_view)


        add_btn.setOnClickListener {
            it.findNavController().navigate(R.id.action_remindersFragment2_to_detailsFragment)
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        mContext = context
        super.onAttach(context)
    }

    override fun onStart() {
        super.onStart()
        (activity as? ToolBarInterface)?.setMyTitle("Hello ${viewModel.getLoggedUser()}")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.profile_menu, menu)
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.options_logout -> {
                val userId = viewModel.getLoggedUserId()
                cancelAllNotifications(userId)
                val i = Intent(mContext, SocketService::class.java)
                mContext.stopService(i)
                viewModel.logoutUser()
                view?.let {
                    Navigation
                        .findNavController(it)
                        .navigate(R.id.action_remindersFragment2_to_loginFragment)
                }
                return true
            }

            R.id.options_profile -> {
                view?.let {
                    Navigation
                        .findNavController(it)
                        .navigate(R.id.action_remindersFragment2_to_profileFragment)
                }
                return true
            }
            else -> {
                return false
            }
        }
    }

    private fun cancelAllNotifications(userId: Long) {
        val alarmManager = activity?.getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, BroadcastReceiver::class.java)
        var pendingIntent: PendingIntent
        viewModel.getUserNotes().observe(viewLifecycleOwner, Observer {
            it?.let {
                val notesList =
                    it.find { it.user.userId == userId }?.notes ?: emptyList()
                for (note in notesList) {
                    pendingIntent = PendingIntent.getBroadcast(
                        context,
                        note.id.toInt(),
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    );
                    alarmManager.cancel(pendingIntent);
                }
            }
        })
    }

    private fun createAllNotifications(userId: Long) {
        viewModel.getUserNotes().observe(viewLifecycleOwner, Observer {
            it?.let {
                val notesList =
                    it.find { it.user.userId == userId }?.notes ?: emptyList()
                val currentDate = Date().time
                for (note in notesList) {
                    if (note.date > currentDate) {
                        (activity as? NotificationInterface)?.setNotification(note)
                    }
                }
            }
        })
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

}
