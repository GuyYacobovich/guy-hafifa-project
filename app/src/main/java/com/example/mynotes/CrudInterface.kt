package com.example.mynotes

import com.example.mynotes.Notes.Note

interface CrudInterface {
        fun add(newNote : Note)
        fun delete(id : Long)
        fun update(newNote: Note)
        fun getList() :List<Note>
}