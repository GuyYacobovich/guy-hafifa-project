package com.example.mynotes.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mynotes.Notes.Note
import com.example.mynotes.R
import kotlinx.android.synthetic.main.note_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class NotesAdapter(private val clickListener: (Note) -> Unit) :
    RecyclerView.Adapter<NotesAdapter.myViewHolder>() {
    private val data: ArrayList<Note> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.note_item, parent, false)
        return myViewHolder(itemView, clickListener)
    }

    fun setData(data: List<Note>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: myViewHolder, position: Int) {
        val note = data[position]
        holder.Bind(note)
    }


    class myViewHolder(itemView: View, val clickListener: (Note) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        var data: Note? = null
        fun Bind(note: Note) {
            data = note
            val locale: Locale = Locale.getDefault()
            val date = Date(note.date)
            var sdf = SimpleDateFormat("h:mm a", locale)
            itemView.note_title.text = note.name
            itemView.note_body.text = note.description
            itemView.note_hour.text = sdf.format(date)
            sdf = SimpleDateFormat("dd/MMMM/yyyy", locale)
            itemView.note_date.text = sdf.format(date)
            sdf = SimpleDateFormat("EEEE", locale)
            itemView.note_day.text = sdf.format(date)
            itemView.note_holder.setOnClickListener {
                clickListener(note)
            }
        }
    }

    override fun getItemCount() = data.size

}
