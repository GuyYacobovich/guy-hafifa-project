package com.example.mynotes.Users

import androidx.room.Embedded
import androidx.room.Relation
import com.example.mynotes.Notes.Note

data class UsersNotes(
    @Embedded val user: User,
    @Relation(
        parentColumn = "userId",
        entityColumn = "userOwnerId"
    )
    val notes : List<Note>
)