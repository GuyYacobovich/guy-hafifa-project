package com.example.mynotes.Users

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.example.mynotes.Users.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user_table WHERE userName = :username")
    fun getUserByUsername(username: String): User?

    @Query("SELECT * FROM user_table WHERE userId = :userId")
    fun getUserById(userId: Long): User?

    @Query("SELECT * FROM user_table")
    fun getAllUsers(): List<User>?

    @Insert(onConflict = REPLACE)
    fun Insert(user: User): Long

    @Transaction
    @Update(onConflict = REPLACE)
    fun Update(user: User)

}