package com.example.mynotes.Users

import android.app.Application
import com.example.mynotes.Notes.NoteDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserRepository @Inject constructor(private val userDao: UserDao) {

    suspend fun insert(user: User): Long {
        return withContext(Dispatchers.IO) { userDao.Insert(user) }
    }

    suspend fun update(user: User) {
        withContext(Dispatchers.IO) { userDao.Update(user) }
    }

    suspend fun getUserByUsername(username: String): User? {
        return withContext(Dispatchers.IO) { userDao.getUserByUsername(username) }
    }

    suspend fun getUserById(userId: Long): User? {
        return withContext(Dispatchers.IO) { userDao.getUserById(userId) }
    }

    suspend fun getAllUsers(): List<User>? {
        return withContext(Dispatchers.IO) { userDao.getAllUsers() }
    }
}