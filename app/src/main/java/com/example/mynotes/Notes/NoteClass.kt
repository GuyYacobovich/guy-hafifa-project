package com.example.mynotes.Notes

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "note_table")
data class Note(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var date: Long = 0,
    var name: String?,
    var description: String?,
    var userOwnerId: Long
)