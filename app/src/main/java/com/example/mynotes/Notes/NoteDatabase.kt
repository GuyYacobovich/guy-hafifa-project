package com.example.mynotes.Notes

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.mynotes.Users.User
import com.example.mynotes.Users.UserDao

class Migration1To2 : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE user_table (userName TEXT NOT NULL, userId INTEGER PRIMARY KEY NOT NULL)")
        database.execSQL("ALTER TABLE note_table ADD COLUMN 'userOwnerId' TEXT NOT NULL DEFAULT'' ")
    }
}
class Migration2To3 : Migration(2, 3){
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE new_note_table (id INTEGER NOT NULL, date INTEGER NOT NULL, name TEXT, description TEXT, userOwnerId INTEGER NOT NULL, PRIMARY KEY('id'))")
        database.execSQL("DROP TABLE note_table")
        database.execSQL("ALTER TABLE new_note_table RENAME TO note_table")
    }
}

@Database(
    entities = [Note::class, User::class],
    version = 3,
    exportSchema = false
)
abstract class NoteDatabase : RoomDatabase() {
    abstract fun noteDao(): NoteDao
    abstract fun userDao(): UserDao


    companion object {
        @Volatile
        private var instance: NoteDatabase? = null

        @JvmField
        val MIGRATION_1_2 = Migration1To2()

        @JvmField
        val MIGRATION_2_3 = Migration2To3()

        fun getInstance(context: Context): NoteDatabase {
            return instance ?: synchronized(this) {
                instance
                    ?: buildDatabase(
                        context
                    ).also { instance = it }
            }
        }

        fun buildDatabase(context: Context): NoteDatabase {
            return Room.databaseBuilder(context, NoteDatabase::class.java, "note_database")
                .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                .build()
        }
    }
}