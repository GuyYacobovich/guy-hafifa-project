package com.example.mynotes.Notes

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.example.mynotes.Users.UsersNotes

@Dao
interface NoteDao {
    @Insert(onConflict = REPLACE)
    fun insert(note: Note):Long

    @Update(onConflict = REPLACE)
    fun update(note: Note)

    @Delete
    fun delete(note: Note)

    @Query("SELECT * FROM note_table WHERE id = :noteId")
    fun findNoteById(noteId: Long): Note?

    @Transaction
    @Query("SELECT * FROM user_table")
    fun UserNotes(): LiveData<List<UsersNotes>?>

    @Query("SELECT * FROM note_table")
    fun getAllNotes():List<Note>?
}