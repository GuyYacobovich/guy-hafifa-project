package com.example.mynotes.Notes

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.mynotes.Dagger.AppComponent
import com.example.mynotes.Dagger.DaggerAppComponent
import com.example.mynotes.Users.UsersNotes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NoteRepository @Inject constructor(private val noteDao: NoteDao) {

    suspend fun insert(note: Note): Long {
        return withContext(Dispatchers.IO) { noteDao.insert(note) }
    }

    suspend fun update(note: Note) {
        withContext(Dispatchers.IO) { noteDao.update(note) }
    }

    suspend fun delete(note: Note) {
        withContext(Dispatchers.IO) { noteDao.delete(note) }
    }

    suspend fun findNoteById(noteId: Long): Note? {
        return withContext(Dispatchers.IO) { noteDao.findNoteById(noteId) }
    }

    suspend fun getAllNotes(): List<Note>? {
        return withContext(Dispatchers.IO) { noteDao.getAllNotes() }
    }

    fun getUserNotes(): LiveData<List<UsersNotes>?> = noteDao.UserNotes()
}