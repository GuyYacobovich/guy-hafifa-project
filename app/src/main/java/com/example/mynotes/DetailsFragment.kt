package com.example.mynotes

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.TimePicker
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.example.mynotes.Notes.Note
import com.example.mynotes.ViewModel.myViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_details.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DetailsFragment : Fragment(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: myViewModel
    val args: DetailsFragmentArgs by navArgs()
    var myNote: Note? = null
    var noteDate = Calendar.getInstance()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val noteId = args.myNoteId
        if (noteId == -1L) {
            inflater.inflate(R.menu.add_menu, menu)
        } else {
            inflater.inflate(R.menu.edit_menu, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(myViewModel::class.java)
        activity?.let {
            viewModel = ViewModelProvider(it, viewModelFactory).get(myViewModel::class.java)
        }
        val noteId = args.myNoteId
        val mPickTimeBtn = btnDate
        val textView = SelectedDate
        val locale: Locale = Locale.getDefault()
        if (noteId >= 0) {
            myNote = viewModel.findNoteById(noteId)
            (activity as? ToolBarInterface)?.setMyTitle(getString(R.string.edit_reminder_title))
            var sdf = SimpleDateFormat("dd/MMMM/yyyy", locale)
            SelectedDate.text = sdf.format(myNote?.date)
            val reminderDesc: TextView = reminder_description
            reminderDesc.text = myNote?.description
            val reminderName: TextView = reminder_name
            reminderName.text = myNote?.name
            sdf = SimpleDateFormat("hh", locale)
            timePicker.currentHour = sdf.format(myNote?.date).toInt()
            sdf = SimpleDateFormat("mm", locale)
            timePicker.currentMinute = sdf.format(myNote?.date).toInt()
        } else {
            (activity as? ToolBarInterface)?.setMyTitle(getString(R.string.add_reminder_title))
        }
        noteDate.set(Calendar.HOUR_OF_DAY, timePicker.currentHour)
        noteDate.set(Calendar.MINUTE, timePicker.currentMinute)
        mPickTimeBtn.setOnClickListener {
            val dpd = DatePickerDialog(
                context!!,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    noteDate.set(year, monthOfYear, dayOfMonth)
                    val sdf = SimpleDateFormat("dd/MMMM/yyyy", locale)
                    textView.text = sdf.format(noteDate.time)
                },
                0,
                0,
                0
            )

            dpd.datePicker.minDate = System.currentTimeMillis()
            dpd.show()
        }
        timePicker.setOnTimeChangedListener { _: TimePicker, hourOfDay: Int, minute: Int ->
            noteDate.set(Calendar.HOUR_OF_DAY, hourOfDay)
            noteDate.set(Calendar.MINUTE, minute)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.options_add -> {
                val name = reminder_name.text.toString()
                val description = reminder_description.text.toString()
                val userId = viewModel.getLoggedUserId()
                viewModel.socketInsertNote(
                    Note(
                        0,
                        noteDate.timeInMillis,
                        description,
                        name,
                        userId
                    )
                )
                Navigation.findNavController(view!!)
                    .navigate(R.id.action_detailsFragment_to_remindersFragment2)
            }
            R.id.options_save -> {
                myNote?.name = reminder_name.text.toString()
                myNote?.description = reminder_description.text.toString()
                myNote?.date = noteDate.timeInMillis
                myNote?.let {
                    viewModel.socketUpdateNote(it)
                }
                Navigation.findNavController(view!!)
                    .navigate(R.id.action_detailsFragment_to_remindersFragment2)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }
}

