package com.example.mynotes

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.mynotes.Notes.Note
import com.example.mynotes.Notifications.NotificationInterface
import com.example.mynotes.Notifications.ReminderBroadcast
import com.example.mynotes.ViewModel.myViewModel
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), ToolBarInterface,
    NotificationInterface, HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: myViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(this, viewModelFactory).get(myViewModel::class.java)

        if (viewModel.socketIsConnected() == false) {
            val i = Intent(application, SocketService::class.java)
            application.startService(i)
        }
        setSupportActionBar(tool_bar_main)
        val navController = this.findNavController(R.id.myNavHostFragment)
        if (viewModel.getLoggedUser() != null) {
            navController.navigate(R.id.remindersFragment2)
        }
        NavigationUI.setupActionBarWithNavController(this, navController)
        createNotificationChannel()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.myNavHostFragment)
        return navController.navigateUp()
    }

    override fun disabelToolBar() {
        tool_bar_main.visibility = View.INVISIBLE
    }

    override fun enableToolBar() {
        tool_bar_main.visibility = View.VISIBLE
    }

    override fun setMyTitle(title: String) {
        tool_bar_main.title = title
    }

    override fun setNotification(note: Note) {
        val intent = Intent(this, ReminderBroadcast::class.java)
        intent.putExtra("NOTE_NAME", note.name)
        intent.putExtra("NOTE_DESCRIPTION", note.description)
        intent.putExtra("NOTE_ID", note.id)
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND)

        val pendingIntent = PendingIntent.getBroadcast(
            this,
            note.id.toInt(),
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val alarmManager =
            getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setExact(
            AlarmManager.RTC_WAKEUP,
            note.date,
            pendingIntent
        )

    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "ReminderChannel"
            val descriptionText = "Notes channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("CHANNEL_ID", name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

}
