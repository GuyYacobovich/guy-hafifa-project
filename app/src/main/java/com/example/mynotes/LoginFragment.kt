package com.example.mynotes

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.mynotes.ViewModel.MyViewModelFactory
import com.example.mynotes.ViewModel.myViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


class LoginFragment : Fragment(),HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: myViewModel
    lateinit var mContext: Context
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        mContext = context
        super.onAttach(context)
    }

    override fun onStart() {
        super.onStart()
        viewModel = ViewModelProvider(this, viewModelFactory).get(myViewModel::class.java)
        activity?.let {
            viewModel = ViewModelProvider(it, viewModelFactory).get(myViewModel::class.java)
        }
        if (viewModel.socketIsConnected() == false) {
            val i = Intent(mContext, SocketService::class.java)
            mContext.startService(i)
        }

        //(activity as? ToolBarInterface)?.disabelToolBar()
        nav_btn.isEnabled = false
        username.addTextChangedListener(
            object : TextWatcher {

                override fun afterTextChanged(s: Editable) {
                    nav_btn.isEnabled = s.toString().isNotBlank()
                }

                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
                ) {
                }
            })


        nav_btn.setOnClickListener{
            val username = username.text.toString().trim()
            viewModel.loginUser(username)
            val user = viewModel.getUserByUsername(username)
            user?.let { viewModel.socketSendUser(it) }
            val userId = viewModel.getUserByUsername(username)?.userId
            userId?.let { viewModel.updateLoggedUserId(it) }
            it.findNavController().navigate(R.id.action_loginFragment_to_remindersFragment2)
        }
    }

    override fun onStop() {
        //(activity as? ToolBarInterface)?.enableToolBar()
        super.onStop()
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

}
