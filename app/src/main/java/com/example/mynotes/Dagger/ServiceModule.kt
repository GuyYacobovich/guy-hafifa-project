package com.example.mynotes.Dagger

import com.example.mynotes.SocketService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    abstract fun contributeSocketService(): SocketService
}