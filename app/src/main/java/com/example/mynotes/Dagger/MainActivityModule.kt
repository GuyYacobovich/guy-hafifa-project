package com.example.mynotes.Dagger

import com.example.mynotes.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
internal abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    abstract fun contributeMainActivity(): MainActivity
}