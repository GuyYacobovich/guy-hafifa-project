package com.example.mynotes.Dagger

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule() {
    @Provides
    @Singleton
    fun providesApplication(application: Application): Application {
        return application
    }
}