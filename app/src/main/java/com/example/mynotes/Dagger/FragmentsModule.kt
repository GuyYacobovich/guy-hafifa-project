package com.example.mynotes.Dagger

import com.example.mynotes.DetailsFragment
import com.example.mynotes.LoginFragment
import com.example.mynotes.ProfileFragment
import com.example.mynotes.RemindersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun contributesLoginFragment(): LoginFragment
    @ContributesAndroidInjector
    abstract fun contributesProfileFragment(): ProfileFragment
    @ContributesAndroidInjector
    abstract fun contributesRemindersFragment(): RemindersFragment
    @ContributesAndroidInjector
    abstract fun contributesDetailsFragment(): DetailsFragment

}
