package com.example.mynotes.Dagger

import android.app.Application
import com.example.mynotes.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    DatabaseModule::class,
    AndroidSupportInjectionModule::class,
    MainActivityModule::class,
    FragmentsModule::class,
    ViewModelModule::class,
    ServiceModule::class])
interface AppComponent {

    fun inject(application: MyApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
        fun build(): AppComponent
    }
    fun inject(fragment: RemindersFragment)
    fun inject(fragment: LoginFragment)
    fun inject(fragment: ProfileFragment)
    fun inject(fragment: DetailsFragment)
    fun inject(activity: MainActivity)
    fun inject(service: SocketService)
}