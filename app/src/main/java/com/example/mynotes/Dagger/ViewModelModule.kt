package com.example.mynotes.Dagger

import androidx.lifecycle.ViewModelProvider
import com.example.mynotes.ViewModel.MyViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindsViewModelFactory(factory: MyViewModelFactory): ViewModelProvider.Factory
}