package com.example.mynotes.Dagger

import android.app.Application
import com.example.mynotes.Notes.NoteDao
import com.example.mynotes.Notes.NoteDatabase
import com.example.mynotes.Notes.NoteRepository
import com.example.mynotes.SharedPref
import com.example.mynotes.SocketRepository
import com.example.mynotes.Users.UserDao
import com.example.mynotes.Users.UserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DatabaseModule(val application: Application) {

    @Singleton
    @Provides
    fun provideDatabase(): NoteDatabase {
        return NoteDatabase.getInstance(
            application
        )
    }

    @Singleton
    @Provides
    fun provideSharedPref(): SharedPref {
        return SharedPref(application)
    }

    @Singleton
    @Provides
    fun provideNoteDao(noteDatabase: NoteDatabase): NoteDao {
        return noteDatabase.noteDao()
    }

    @Singleton
    @Provides
    fun provideUserDao(noteDatabase: NoteDatabase): UserDao {
        return noteDatabase.userDao()
    }

    @Singleton
    @Provides
    fun ProvideUserRepository(userDao: UserDao): UserRepository {
        return UserRepository(userDao)
    }

    @Singleton
    @Provides
    fun ProvideNoteRepository(noteDao: NoteDao): NoteRepository {
        return NoteRepository(noteDao)
    }

    @Singleton
    @Provides
    fun ProvideSocketRepository(): SocketRepository {
        return SocketRepository()
    }
}
