package com.example.mynotes

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.mynotes.Users.User
import com.example.mynotes.ViewModel.MyViewModelFactory
import com.example.mynotes.ViewModel.myViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_profile.*
import javax.inject.Inject


class ProfileFragment : Fragment(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: myViewModel
    var username: String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        (activity as? ToolBarInterface)?.setMyTitle(getString(R.string.profile_title))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this, viewModelFactory).get(myViewModel::class.java)
        username = viewModel.getLoggedUser()
        edit_user.setText(username)
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.profile_activity_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.options_activity_save -> {
                val user = viewModel.getUserById(viewModel.getLoggedUserId())
                user?.userName = edit_user.text.toString().trim()
                user?.let {
                    viewModel.updateUser(it)
                    viewModel.updateLoggedUser(it.userName)
                }
                view?.let {
                    Navigation.findNavController(it)
                        .navigate(R.id.action_profileFragment_to_remindersFragment2)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }
}
