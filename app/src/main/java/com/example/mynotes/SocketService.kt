package com.example.mynotes

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.mynotes.Notes.Note
import com.example.mynotes.Notes.NoteRepository
import com.example.mynotes.Users.User
import com.example.mynotes.Users.UserRepository
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.nkzawa.emitter.Emitter
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class SocketService : HasAndroidInjector, Service() {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var noteRepository: NoteRepository
    @Inject
    lateinit var userRepository: UserRepository
    @Inject
    lateinit var socketRepository: SocketRepository

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        socketRepository.addListener("note_added", newNoteListener)
        socketRepository.addListener("note_updated", updateNoteListener)
        socketRepository.addListener("note_deleted", noteDeleteListener)
        socketRepository.addListener("user_added", newUserListener)
        socketRepository.connect()
        val notesList = runBlocking { noteRepository.getAllNotes() }
        socketRepository.sendAllNotesToServer(ObjectMapper().writeValueAsString(notesList))
        val usersList = runBlocking { userRepository.getAllUsers() }
        socketRepository.sendAllUsersToServer(ObjectMapper().writeValueAsString(usersList))
        return super.onStartCommand(intent, flags, startId)
    }


    private val newNoteListener = Emitter.Listener { args ->
        val om = ObjectMapper()
        val note = om.readValue(args[0].toString(), Note::class.java)
        GlobalScope.launch { noteRepository.insert(note) }
    }

    private val updateNoteListener = Emitter.Listener { args ->
        val om = ObjectMapper()
        val note = om.readValue(args[0].toString(), Note::class.java)
        GlobalScope.launch { noteRepository.update(note) }
    }

    private val newUserListener = Emitter.Listener { args ->
        val om = ObjectMapper()
        val data = om.readValue(args[0].toString(), User::class.java)
        GlobalScope.launch { userRepository.insert(data) }
    }

    private val noteDeleteListener = Emitter.Listener { args ->
        val om = ObjectMapper()
        val note = om.readValue(args[0].toString(), Note::class.java)
        GlobalScope.launch { noteRepository.delete(note) }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun onDestroy() {
        socketRepository.disconnect()
        super.onDestroy()
    }
}