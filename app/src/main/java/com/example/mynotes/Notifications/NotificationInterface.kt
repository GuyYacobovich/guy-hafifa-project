package com.example.mynotes.Notifications

import com.example.mynotes.Notes.Note

interface NotificationInterface{
    fun setNotification(note: Note)
}