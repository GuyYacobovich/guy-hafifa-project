package com.example.mynotes.Notifications

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.mynotes.MainActivity
import com.example.mynotes.R

class ReminderBroadcast : BroadcastReceiver() {
    override fun onReceive(context: Context?, intentExtra: Intent?) {
        val noteExtra = intentExtra?.extras
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent? = noteExtra?.getLong("alarm_id")?.toInt()
            ?.let { PendingIntent.getActivity(context, it, intent, 0) }

        context?.let {
            val builder = NotificationCompat.Builder(it, "CHANNEL_ID")
                .setSmallIcon(R.drawable.reminder_icon)
                .setContentTitle(noteExtra?.getString("NOTE_NAME"))
                .setContentText(noteExtra?.getString("NOTE_DESCRIPTION"))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)

            val notificationManager: NotificationManagerCompat =
                NotificationManagerCompat.from(context)
            noteExtra?.getLong("NOTE_ID")?.toInt()
                ?.let { notificationManager.notify(it, builder.build()) }
        }

    }
}
