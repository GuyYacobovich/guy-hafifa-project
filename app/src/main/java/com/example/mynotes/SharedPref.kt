package com.example.mynotes

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class SharedPref @Inject constructor(application: Application) {
    private val sharedPref: SharedPreferences =
        application.getSharedPreferences(
            application.getString(R.string.shared_pref),
            Context.MODE_PRIVATE
        )
    private val editor = sharedPref.edit()

    fun loginUser(username: String) {
        editor.putString("USERNAME", username)
        editor.apply()
    }

    fun updateLoggedUsername(username: String) {
        editor.putString("USERNAME", username)
        editor.apply()
    }

    fun logoutUser() {
        editor.putString("USERNAME", null)
        editor.putLong("USERID", 0)
        editor.apply()
    }

    fun getLoggedUser(): String? {
        return sharedPref.getString("USERNAME", null)
    }

    fun updateLoggedUserId(userId: Long) {
        editor.putLong("USERID", userId)
        editor.apply()
    }

    fun getLoggedUserId(): Long{
        return sharedPref.getLong("USERID", 0)
    }

//    TODO ask yohai aboout this (already using Dagger singleton annotation)
//    companion object {
//        fun getInstance(application: Application): SharedPref {
//            return SharedPref(application)
//        }
//    }

}