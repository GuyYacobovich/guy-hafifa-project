
const io = require('socket.io')(2000);
var users = []
var notes = []
io.on('connection', (socket) => {
    console.log("connected")
    const mergeNotesById = (a1, a2) =>
    a1.map(itm => ({
    ...a2.find((item) => (item.id === itm.id) && item),
    ...itm
     }));
    const mergeUsersById =(a1, a2) => a1.map(itm => ({
        ...a2.find((item) => (item.userId === itm.userId) && item),
        ...itm
    }));
    socket.on('setup_users', (usersList) => {
        usersParsed = JSON.parse(usersList)
        users = mergeNotesById(users, usersParsed)
    })
    socket.on('setup_notes', (notesList) =>{
        let notesParsed = JSON.parse(notesList)
        notes = mergeUsersById(notes,notesParsed)
    })
    socket.on('insert_user', (addUser) => {
        let isUser = false
        const newUser = JSON.parse(addUser)
        for (user of users) {
            if (user.userName == newUser.userName)
                isUser = true
        }
        if (!isUser) {
            users.push(newUser)
        }
        console.log(`${newUser.userName} is logged as ${socket.id}`)
        socket.broadcast.emit("user_added", {
            user: newUser
        })
    })

    socket.on('insert_note', (addNote) => {
        const note = JSON.parse(addNote)
        notes.push(note)
        console.log(`${socket.id} added note`)
        socket.broadcast.emit("note_added", {
            note: note
        })
    })

    socket.on('update_note', (updatedNote) => {
        const note = JSON.parse(updatedNote)
        for (i in notes) {
            if (notes[i].noteId == note.noteId) {
                notes[i] = note
            }
        }
        console.log(`${socket.id} updated a note`)
        socket.broadcast.emit("note_updated", {
            note: note
        })
    })

    socket.on('delete_note', (noteToDeleted) => {
        const note = JSON.parse(noteToDeleted)
        for (i in notes) {
            if (notes[i].noteId == note.noteId) {
                notes.splice(i, 1)
            }
        }
        console.log(`${socket.id} deleted a note`)
        socket.broadcast.emit("note_deleted", {
            note: note
        })
    })
    socket.on('disconnect', (socket) => {
        console.log(`Socket disconnected`)
    })
});


